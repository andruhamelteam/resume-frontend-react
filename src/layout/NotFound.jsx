import React from 'react';

const NotFound = ({location}) => {
  return (
    <div>
      {`The route ${location.pathname} not found.`}
    </div>
  )
}

export default NotFound;