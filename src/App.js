import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import NavBar from './layout/NavBar';
import NotFound from './layout/NotFound';
import Home from './pages/Home';

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <NavBar />
        <div className='container'>
          <div className='row'>
            <div className='col'>
              <Switch>
                <Route exact path='/' component={Home} />
                <Route path='/resume/:id' render={(props) => {
                  return (
                    <React.Fragment>
                      <h1>Route showing single resume for public.</h1>
                      <h3>{`Requested resume id: ${props.match.params.id}`}</h3>
                    </React.Fragment>
                  );
                }} />
                <Route path='/user/dashboard' render={() => {
                  return (
                    <h1>Route showing currently logged in User's dashboard.
                      Must be not accesible for others.
                    </h1>
                  );
                }} />
                <Route component={NotFound} />
              </Switch>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default App;
